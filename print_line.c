#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>
#include <sys/ioctl.h>

#define STR_MAX_LEN 200

bool isEjiChar(unsigned int codepoint) {
  // Range of Unicode characters reserved for emojis
  return (codepoint >= 0x1F600 && codepoint <= 0x1F64F) ||
         (codepoint >= 0x1F300 && codepoint <= 0x1F5FF) ||
         (codepoint >= 0x1F680 && codepoint <= 0x1F6FF) ||
         (codepoint >= 0x2600 && codepoint <= 0x26FF) ||
         (codepoint >= 0x2700 && codepoint <= 0x27BF) ||
         (codepoint >= 0x1F900 && codepoint <= 0x1F9FF) ||
         (codepoint >= 0x1F1E6 && codepoint <= 0x1F1FF);
}

int main(int argc, char *argv[]) {
  if (argc != 4) {
    printf("Usage: ./program <left_text> <right_text> <div>\n");
    return 1;
  }

  struct winsize w;
  ioctl(0, TIOCGWINSZ, &w);
  
  char *left_text = argv[1];
  char *right_text = argv[2];
  int divider = atoi(argv[3]);
  int max_width = w.ws_col;
  
  // Printable character limits
  int max_left = divider;
  int max_right= max_width - max_left;
  
  /*
     1. iterate left up N chars that correspond to max_left printable chars
     2. make note of any starting colour sequences, and terminate them so that right_text doesn't have to.
     3. Use those colour sequences as the start of the next line/run
     4. Print left

     5. iterate right up M chars that correspond to max_right printable chars
     6. make note of any starting colour sequences, and terminate them so that left_text's next line doesn't have to
     7. use those colour sequences as the start of the next run
     8. Print right

     9. Print newline
     10. Repeat until left and right iterators are zero.

   */
 
  int li=0, lj=0, ri=0, rj=0, /* iterators */
      li_last=0, ri_last = 0,
      lp=0, rp=0;  /* printable */

  char left_format[10]; 
  char right_format[10];
  sprintf(left_format, "%%%ds ", (max_left - 1));
  sprintf(right_format, "%%-%ds", max_right);

  // -- Handle current line
  while (true){
    li_last = li;

    // -- Handle Left
    unsigned int codepoint;
    unsigned int last_space = -1;
    while(lp++ < max_left){
      int bytes = 0;
      if ((left_text[li] & 0x80) == 0x00)
        bytes = 1; // 1-byte UTF-8 character
      else if ((left_text[li] & 0xE0) == 0xC0)
        bytes = 2; // 2-byte UTF-8 character
      else if ((left_text[li] & 0xF0) == 0xE0)
        bytes = 3; // 3-byte UTF-8 character
      else if ((left_text[li] & 0xF8) == 0xF0)
        bytes = 4; // 4-byte UTF-8 character

      if (bytes == 0) {
        break; // Invalid UTF-8 sequence
      }

      if (isspace(left_text[li])){
        last_space = li;
      }
      // Emo check
      /* codepoint = left_text[li] & (0xFF >> (bytes + 1)); */
      /* for (int k = 1; k < bytes; ++k) { */
      /*   codepoint = (codepoint << 6) | (left_text[li + k] & 0x3F); */
      /* } */
      /* if (!isEjiChar(codepoint)) { */
      /*   for (int k = 0; k < bytes; ++k) { */
      /*     left_text[lj++] = left_text[li++]; */
      /*   } */
      /* } else { */
        li += bytes;
      /* } */
    }
    // Break on space if distance is short
    if (li - last_space < 4){
      li = last_space;
    }
    lp=0;
    int left_piece = li - li_last;
    char substr_left [left_piece];
    strncpy(substr_left, left_text + li_last, left_piece);      // dest, orig, offset, len
    printf(left_format, substr_left);

    li_last = li;

    /* // -- Handle Right */
    while (rp++ < max_right){
      int bytes = 0;
      if ((right_text[ri] & 0x80) == 0x00)
        bytes = 1; // 1-byte UTF-8 character
      else if ((right_text[ri] & 0xE0) == 0xC0)
        bytes = 2; // 2-byte UTF-8 character
      else if ((right_text[ri] & 0xF0) == 0xE0)
        bytes = 3; // 3-byte UTF-8 character
      else if ((right_text[ri] & 0xF8) == 0xF0)
        bytes = 4; // 4-byte UTF-8 character

      if (bytes == 0) {
        break; // Invalid UTF-8 sequence
      }

      // Emoji check
      codepoint = right_text[ri] & (0xFF >> (bytes + 1));
      for (int k = 1; k < bytes; ++k) {
        codepoint = (codepoint << 6) | (right_text[ri + k] & 0x3F);
      }
      if (isEjiChar(codepoint))
        ri += bytes;
    }
    rp = 0;
    int right_piece = ri - ri_last;
    char substr_right [right_piece];
    // dest, orig, offset, len
    strncpy(substr_right, right_text + ri_last, right_piece);
    //printf(right_format, substr_right);

    ri_last = ri;

    if ((li >= strlen(left_text)) || (ri >= strlen(right_text))){
      break;
    }
    printf("\n");
  }
  printf("\n");
  return 0;
}
