# Print Line

This aims to be a teeny tiny C program that can take two pieces of text as input and format them to a fixed width, regardless of any colouring tags or emojis in the content.

Current utilities such as fold, fmt, and others have great flexibility when it comes to the word folding capabilities (padding, breaking on spaces, etc.), but seem to equally fail when it comes to correctly guessing the size of the printed string that has been coloured or contains emojis.

Example:

```
  ./print_line "**long fancy title text probably in bold**" "Normal plain text that probably has some *colour* information in there, as well as 😂 some random emojis 🧘🏻‍♂️, 🌍, 🌦️, 🥖, 🚗, 📱."  15
```

 
 ```
  **long fancy title** : Normal plain text that probably has some *col*  
  **text probably in**   *our* information in there, as well as 😂 some  
              **bold**   random emojis 🧘🏻‍♂️, 🌍, 🌦️, 🥖, 🚗, 📱.
```


Once fully developed, this should be a drop in solution to DietPi's Banner Line Wrapping: https://github.com/MichaIng/DietPi/pull/5820/
